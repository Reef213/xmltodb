#Создание базы данных

#create database test

#use test

#Создание тестовой таблицы

create table work (
ID INT(11) UNSIGNED AUTO_INCREMENT,
DepCode char(20) NOT NULL,
DepJobe char(100) NOT NULL,
Description char(255),
PRIMARY KEY(ID));

#Описание данных таблицы

INSERT INTO work (DepCode,DepJobe,Description ) VALUES (
'0003','Tester',
'No coments'
),(
'0052','Web-Coder',
'0454'
),(
'0051','Web-Designer',
'0%%005'
),(
'0050','SEO',
'0005'
),(
'0001','Programmer',
'0001'
),(
'0002','Coder',
'0002'
)

