package top;

import java.util.HashMap;

class StructureXMLFile {
	
	public HashMap<String,String> flDescriptionHash  =new HashMap<String,String>();
	public HashMap<String,String> flDepCodeHash  =new HashMap<String,String>();
	public HashMap<String,String> flDepJobHash  =new HashMap<String,String>();
	
	public final String rootName="box";
	public final String subRootName="workman";
	public final String elDepartmentName="department";
	public final String atrDepCodeName="DepCode";
	public final String atrDepJobName="DepJob";
	public final String elDescriptionName="Description";
}
