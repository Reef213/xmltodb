package top;
/**
 * @autor Reef
 * @since JDK 1.7.0
 * 
 * */

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import top.StructureDataBase;
import top.StructureXMLFile;
import top.MakeDataBase;
//import top.Settings;


public final class MyFacade 
{	
	StructureDataBase dbStructure;
	StructureXMLFile flStructure;
	MakeDataBase dbFunc;
	MakeDataBase bound_db;
	
	//������ ����������
	//Connection connection;
		
/**����� setFiletoDB() �������� ������ �� ����� � ��������� � ���� ������
 * @param filename ��� ����� ������� ����� ������  
 * @param connection_db ������ �� Connection ��� ���������� � ����� ������
 * @throws Exception 
 * @see java.xml.*
 * @see top.MakeDataBase
 * */


public void setup()
{
	bound_db=new MakeDataBase();
	bound_db.connection();
}

public void showSettings()
{
	Settings setteing=new Settings();
	setteing.show_settings();
}
	
public void addtoDB(String filename) throws Exception
{
	flStructure=new StructureXMLFile();
	dbFunc=new MakeDataBase();
	ResultSet dbData=null;
	
	dbStructure=new StructureDataBase();
	
	dbData=dbFunc.getExecuteQuery(dbStructure.SqlQuerySelectAll);
	//�� ����������� ������� � ������� dbData ��������� �������� ������� ���� ������
	
	String keydb;
	
	while(dbData.next())
	{
		keydb= dbData.getString(2)+dbData.getString(3);
		System.out.println("kdb="+dbData.getString(2));
		dbStructure.dbDepCodeHash.put(keydb,dbData.getString(2));
		dbStructure.dbDepJobHash.put(keydb,dbData.getString(3));
		dbStructure.dbDescriptionHash.put(keydb,dbData.getString(4));	
	}
	
		flStructure=getDatatoFile(flStructure,filename);
		dbFunc.updateDB(dbStructure,flStructure);
		dbFunc.deleteDB(dbStructure,flStructure);
		dbFunc.insertDB(dbStructure,flStructure);
		bound_db.close();
}

/**����� addtoFile() �������� ������ �� ���� ������ � ��������� �� � ����
 * @param filename ��� ����� ������� ����� ������  
 * @param connection_db ������ �� Connection ��� ���������� � ����� ������
 * @see java.xml.*
 * @see top.MakeDataBase
 * */

public void addtoFile(String filename) throws DOMException, SQLException
{
		bound_db.close();
}

private DocumentBuilder getBuildDoc(){
	DocumentBuilderFactory documentBuiderFactory=DocumentBuilderFactory.newInstance();
	DocumentBuilder ret=null;
	try {
		ret=documentBuiderFactory.newDocumentBuilder();
	} catch (ParserConfigurationException e) {
		e.printStackTrace();
	}
	return(ret);
}

private void saveFile(Document document,String filename)
{
	TransformerFactory trFactory=TransformerFactory.newInstance();
	Transformer transformer=null;
	StreamResult result=null;
	try {
		transformer = trFactory.newTransformer();
	} catch (TransformerConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		DOMSource source=new DOMSource(document);
		//������� ������ result ������� ����� ���������� ������ � filename	
		try {
			result = new StreamResult(new FileWriter(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//��������� ������ �� ��������� � ������� result 
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
}

private StructureXMLFile getDatatoFile(StructureXMLFile flStructure,String filename) throws Exception
{	
	//�������������� ������ checking ��������� ��� �������� ������������ ������ ����a
	HashSet<String> checking=new HashSet<String>();
	
	DocumentBuilder documnet_builder=getBuildDoc();
	Document document=null;
	
	try 
	{
		
		document = documnet_builder.parse(filename);
		
	} catch (SAXException e1) {
		
		e1.printStackTrace();
		
	} catch (IOException e1) {
		
		e1.printStackTrace();
		
	}
	Element root_document=document.getDocumentElement();

	NodeList list_root_child=root_document.getChildNodes();

	int count_child_root=list_root_child.getLength();
	NodeList list_sub_child;

	String key="",str="";
	
	for(int i=0;i<count_child_root;i++)
	{
		if(list_root_child.item(i).getNodeName()==dbStructure.TableName)
		{
			list_sub_child=list_root_child.item(i).getChildNodes();
			int count_sub_child=list_sub_child.getLength();
			
		for(int j=0;j<count_sub_child;j++)
			{
				if(list_sub_child.item(j).getNodeName()==flStructure.elDepartmentName)
				{	
					//���������� key �����, ������� �� ���� ��������
						key=list_sub_child.item(j).getAttributes().getNamedItem(flStructure.atrDepCodeName).getTextContent()+
							list_sub_child.item(j).getAttributes().getNamedItem(flStructure.atrDepJobName).getTextContent();
						String str1=list_sub_child.item(j).getAttributes().getNamedItem(flStructure.atrDepCodeName).getTextContent();
						String str2=list_sub_child.item(j).getAttributes().getNamedItem(flStructure.atrDepJobName).getTextContent();
						flStructure.flDepCodeHash.put(key,str1);
						flStructure.flDepJobHash.put(key, str2);
						
				} else if(list_sub_child.item(j).getNodeName()==flStructure.elDescriptionName)
				{
					/*�������� �����, ���� ��������� ��� ���������� �����
					 * ������� ���������� ���� ������
					*/
					if(checking.add(key))
					{
						str=list_sub_child.item(j).getTextContent();
						flStructure.flDescriptionHash.put(key,str);
						
					}else
					{
						throw new Exception("Duplicate key to="+filename);
					}
				}
				
			}
		}
	}
	
	return(flStructure);
}

}
