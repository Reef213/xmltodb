package top;
/**
* @autor Reef
* @since JDK 1.7.0
* 
*/

import java.io.*;
import java.util.Properties; 

/**����� PropCreate ������������ ���� ��������� ��� ������ ���������
 * ����� read_Prop_File ��������� ��������� � �����
 * ����� show_Prop_File ���������� ���������� ����� ��������.
 * ������ �������� � ���� �� �������������.
 * ��������� �� ���������
 * 	url="127.0.0.1:3306";
 *	user="root";
 *	pass="root";
 *
 * @see java.util.Properties;
 * */


public final class Settings{
	private String url,
				user,
				pass;
	private static String defaultUrl="127.0.0.1:3306",
				   defaultUser="root",
				   defaultPass="root";
					
	private String filename="prop.txt";
	
/**����� read_Prop_File ��������� ��������� � �����.
* ���� ���������� ����� �� �������, �� ��������������� ��������� �� ���������.
*  ��������� �� ���������
* 		url="127.0.0.1:3306";
*		user="root";
*		pass="root";
* @see java.util.Properties;
* 
* 
*/
	
public void setup()
	{try{
			FileReader file_properties=new FileReader(filename);
			Properties body_file_properties=new Properties();
			body_file_properties.load(file_properties);
			url  ="jdbc:mysql://"+body_file_properties.getProperty("url")+"/test";
			user =body_file_properties.getProperty("user");
			pass =body_file_properties.getProperty("password");
			 file_properties.close();
		}
		catch(IOException ee){
			System.out.println("Configuration file could not be read.The default settings");
			System.out.println("url=127.0.0.1:3306");
			System.out.println("user=root");
			System.out.println("password=root");
			url=defaultUrl;
			user=defaultUser;
			pass=defaultPass;
		}	
	}
/**
 * url ���������� ����� ����� ���� ������.
 * @return String url
**/

String url()
	{
		return(url);
	}
/**
 * urser ���������� ��� ������������ ���� ������.
 * @return String user
**/

String user()
	{
		return(user);
	}
/**
 * pass ���������� ������ ��� ����� � ���� ������.
 * @return String pass
**/

String pass()
	{
		return(pass);
	}
	
/**����� show_Prop_File ���������� ��������� � �����.
* ���� ���������� ����� �� �������, �� ������������ ��������� �� ���������.
*  ��������� �� ���������
* 		url="127.0.0.1:3306";
*		user="root";
*		pass="root";
* @see java.util.Properties;
* 
*/

public void show_settings()
	{
		try{
			FileReader file_properties=new FileReader(filename);
			Properties body_file_properties=new Properties();
			body_file_properties.load(file_properties);
			System.out.println("Configuration file show");
			System.out.println("url="     +body_file_properties.getProperty("url"));
			System.out.println("user="    +body_file_properties.getProperty("user"));
			System.out.println("password="+body_file_properties.getProperty("password"));
			file_properties.close();
		}
		catch(IOException ee){
			System.out.println("Configuration file could not be read.The default settings");
			System.out.println("url=127.0.0.1:3306");
			System.out.println("user=root");
			System.out.println("password=root");
		}	
	};

}
