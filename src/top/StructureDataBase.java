package top;

import java.util.HashMap;

public class StructureDataBase {
	
	public HashMap<String,String> dbDescriptionHash  =new HashMap<String,String>();
	public HashMap<String,String> dbDepCodeHash  =new HashMap<String,String>();
	public HashMap<String,String> dbDepJobHash  =new HashMap<String,String>();
	
        public final String BaseName="test";
        
	public final String TableName="work";
	
	public final String colDepCode="DepCode";
	public final String colDepJob="DepJob";
	public final String colDescription="Description";
	public final String SqlQuerySelectAll="SELECT * FROM "+TableName;
	
	public String sqlQueryUpdate(StructureXMLFile flStructure,String key)
	{
		String query;
		query="UPDATE `"+BaseName
				+"`.`"+TableName+"` SET `"+colDescription+"`='"+
				flStructure.flDescriptionHash.get(key)+"' WHERE  `DepCode`=\""+dbDepCodeHash.get(key)+"\" && `DepJob`=\""+dbDepJobHash.get(key)+"\";";
		return(query);
	}
	
	public String sqlQueryDelete(String key)
	{
		String query;
		query="DELETE FROM `"+BaseName
				+"`.`"+TableName+"` WHERE  `DepCode`=\""+dbDepCodeHash.get(key)+"\" && `DepJob`=\""+dbDepJobHash.get(key)+"\";";
		return(query);
	}
	
	public String sqlQueryInsert(StructureXMLFile flStructure,String key)
	{
		String query;
		query="INSERT INTO "+
				TableName+"("+
				colDepCode+","+
				colDepJob+","+
				colDescription+
				") VALUES ('"+
				flStructure.flDepCodeHash.get(key)+"','"+
				flStructure.flDepJobHash.get(key)+"','"+
				flStructure.flDescriptionHash.get(key)+"');";
		
		return(query);
	}
}
