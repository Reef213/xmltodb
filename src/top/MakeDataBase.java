package top;
/**
* @autor Reef
* @since JDK 1.7.0
* 
*/
import java.sql.*;
import java.util.Map;
import java.util.Set;

import top.Settings;
import top.StructureDataBase;



/**����� MakeDataBase ������� � ��������� ���������� � ����� �����
 * ����� connection() ������������ ���������� � ����� ������ � ��������� ��������� �� ��������
 * ��� �� �������� ����������.
 * ����� close() ��������� ���������� � ����� ������ � ��������� ��������� �� ��������
 * ��� �� �������� �������� ����������.
 * ����� get_link_Connectio() ���������� ����� ��������� ����������
 * */


public class MakeDataBase {
	private StructureDataBase dbStructure;
	private Connection conn;
	private Connection connection_db;
	Settings setting;
	
/**
 * ����� connection ������������ ���������� � ����� ������ � ��������� ��������� �� ��������
 * ��� �� �������� ����������
 * @param url ����� ����� ���� ������.
 * @param user ��� ������������ ��� ������� � ��������� ���� ������
 * @param pass ������ ��� ������� � ��������� ���� ������
 * @see getConnestion
 * @see java.sql.*;
 * */

	
	
public void connection()
	{
	setting= new Settings();
	dbStructure=new StructureDataBase();
		try{
			connection_db = DriverManager.getConnection(setting.url(),
					setting.user(),
					setting.pass());
			System.out.println("Connection host:"+setting.url());
			}
		catch(SQLException ee){
			System.out.println("Don't Connection host:"+setting.url());
			System.out.println("Errore:"+ee);
		}
	}
/**
 * ����� get_link_Connection ���������� ����� ��������� ����������
 * @return conn
 * */

Connection get_link_Connection()
	{
		return(conn);
	}

/**����� close() ��������� ���������� � ����� ������ � ��������� ��������� �� ��������
 * ��� �� �������� �������� ����������.
 * @param  connection ���������� ����� ��������� ����������
 * */
	
public void close()
	{
		try
		{connection_db.close();
		System.out.println("Connection close");
		}catch(SQLException ee){
			System.out.println("Connection not close"+ee);
		}	
	}


public ResultSet getExecuteQuery(String sqlquery) 
	{	
		Statement query_db=getState();
		ResultSet ret=null;
		try {
			ret=query_db.executeQuery(sqlquery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(ret);
	}

public Statement getState()
	{
		Statement query_db=null;
		try {
			query_db=connection_db.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(query_db);
	}

public void updateDB(
		StructureDataBase dbStructure,
		StructureXMLFile flStructure) throws SQLException
{
	Statement query_db=getState();
	Set<Map.Entry<String,String>> set1=dbStructure.dbDescriptionHash.entrySet();
	for(Map.Entry<String,String> me :set1)
	{
		if(!flStructure.flDescriptionHash.containsKey(me.getKey()))
		{	
			query_db.executeUpdate(dbStructure.sqlQueryDelete(me.getKey()));
		}
	}
	
}

public void deleteDB(
		StructureDataBase dbStructure,
		StructureXMLFile flStructure
		) throws SQLException
{
	Statement query_db=getState();
	Set<Map.Entry<String,String>> set1=dbStructure.dbDescriptionHash.entrySet();
	for(Map.Entry<String,String> me :set1)
	{
		if(flStructure.flDescriptionHash.containsKey(me.getKey()))
		{	
			query_db.executeUpdate(dbStructure.sqlQueryUpdate(flStructure,me.getKey()));
		}
	}
	
}

public void insertDB(
		StructureDataBase dbStructure,
		StructureXMLFile flStructure) throws SQLException
	{
	Statement query_db=getState();
	Set<Map.Entry<String,String>> set2=flStructure.flDescriptionHash.entrySet();
	//��������� �������� INSERT
	System.out.println("point_2");
	for(Map.Entry<String,String> me:set2)
	{	
		if(!dbStructure.dbDescriptionHash.containsKey(me.getKey())){
				query_db.executeUpdate(dbStructure.sqlQueryInsert(flStructure,me.getKey()));

		}
		
	}		
}
}