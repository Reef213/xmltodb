package top;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MakeFile{

private MakeDataBase dbFunc;

	
public void MakeFile(StructureXMLFile flStructure, StructureDataBase dbStructure,String filename) throws DOMException, SQLException
{
	dbFunc = new MakeDataBase();
	ResultSet dbData=null;
	DocumentBuilder documentBuilder=getBuildDoc();
	
	dbData=dbFunc.getExecuteQuery(dbStructure.SqlQuerySelectAll);
 	
	//������� ����� ��������
 	org.w3c.dom.Document document=(Document) documentBuilder.newDocument();
 	//������������� �������� �������
 	Element rootElemnt=document.createElement(flStructure.rootName);
 	//������ �������� ������� �������� �������� ���������
 	document.appendChild(rootElemnt);
 	//�� ����������� ������� � ������� dbData ��������� �������� ������� 
 	while(dbData.next())
	{
 		Element emWorkman=document.createElement(dbStructure.TableName);
 		rootElemnt.appendChild(emWorkman);
 		Element emStr1=document.createElement(flStructure.elDepartmentName);
 		emStr1.setAttribute(flStructure.atrDepCodeName, dbData.getString(2));
 		emStr1.setAttribute(flStructure.atrDepJobName,dbData.getString(3));
 		Element emStr2=document.createElement(flStructure.elDescriptionName);
 		emStr2.appendChild(document.createTextNode(dbData.getString(4)));
 		emWorkman.appendChild(emStr1);
 		emWorkman.appendChild(emStr2);
		}
 	saveFile(document, filename);
 
}

private DocumentBuilder getBuildDoc(){
	DocumentBuilderFactory documentBuiderFactory=DocumentBuilderFactory.newInstance();
	DocumentBuilder ret=null;
	try {
		ret=documentBuiderFactory.newDocumentBuilder();
	} catch (ParserConfigurationException e) {
		e.printStackTrace();
	}
	return(ret);
}

	
private void saveFile(Document document,String filename)
{
	TransformerFactory trFactory=TransformerFactory.newInstance();
	Transformer transformer=null;
	StreamResult result=null;
	try {
		transformer = trFactory.newTransformer();
	} catch (TransformerConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		DOMSource source=new DOMSource(document);
		//������� ������ result ������� ����� ���������� ������ � filename	
		try {
			result = new StreamResult(new FileWriter(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//��������� ������ �� ��������� � ������� result 
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
}


}